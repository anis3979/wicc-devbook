# Smart contract complement

<extoc></extoc>

---

## GetTxRegID
Get the transaction originating account of the specified hash

**Prototype of function**

`mylib.GetTxRegID(hashTbl)`

**Parameters**

| parameter | type |
|:-------:|:-----:|
| Trading hash | table |


**Returns**

Success: return 6 bytes of account ID

Failure: nil

**Example**
```lua
Function mylib_GetTxRegID()
    Local hash = {0x24, 0x4f, 0xa7, 0xcf, 0x97, 0xae, 0x15, 0x85, 0xd8, 0xf8, 0x02, 0x4b, 0xa1, 0x8b, 0x8a, 0xbe, 0xce, 0x8e, 0xb9, 0xcd, 0x4d, 0x01, 0x6d, 0xd0, 0xba, 0x8c, 0xc0, 0xdc, 0x85, 0x1a, 0x9c, 0x0e}
    Local accounts = {mylib.GetTxRegID(Unpack(hash))}
    LogMsg(LOG_TYPE.ENUM_NUMBER,# accounts, accounts)
End
```

---

## GetAccountPublickey
Get the public key of the account

**Prototype of function**

`mylib.GetAccountPublickey(Unpack(accountTbl))`

**Parameters**

6-byte account ID

**Returns**


|return | type | example |destination|
| :---- | :---- | :----: | :----: |
Publickey | table | {0x03,0x6c,0x53,0x97,0xf3,0x22,<br>0x7a,0x1e,0x20,0x99,0x52,0x82,0x9d,<br>0x24,0x9b, 0x7a,0xd0,0xf6,0x15, 0xe4,0x3b,<br>0x76,0x3a,0xc1,0x5e,0x3a,0x6f,<br>0x52,0x62,0x7a,0x10,0xdf,0x21} | success (33 bytes) |
| nil | | | failed |


**Example**
```lua
Function mylib_GetAccountPublickey()
    Local accountTbl = {mylib.GetCurTxAccount()}
    Local i
    Local result = {mylib.GetAccountPublickey(Unpack(accountTbl))}
    Assert(#result == 33,"GetAccountPublickey err");
    For i = 1,#result do
        Print("Publickey",i,(result[i]))
    End
End
```


---

## GetBlockHash
Get the block hash based on the block height

**Prototype of function**

`mylib.GetBlockHash(height)`

**Parameters**

| parameter | type |
|:-------:|:-----:|
| Block Height | Number |


**Returns**

|return | type | example |destination|
| :---- | :---- | :---- | :--- |
| height | table | {0x46,0x78,0xd3,0xc5,0x4c,0xaf,0x3b,0xd7,0xaf,<br>0x59,0x1d,0xb4,0x85,0x21,0xf9,0xb1,0xf6,<br>0x98,0x12, 0x90,0x9c,0x56,0x19,0xba,0x7a,<br>0x46,0x22,0x6b,0xb7,0x2b,0x61,0xd0} | success |
| nil | | | failed |

**Example**
```lua
Function mylib_GetBlockHash()
    Local height = 47037
    Local i
    Local result = {mylib.GetBlockHash(height)}
    Assert(#result == 32,"GetBlockHash err");
    For i = 1,#result do
        Print("BlockHash",i,(result[i]))
    End
End
```

---

## GetCurRunEnvHeight
Get the current running height

**Prototype of function**

`mylib.GetCurRunEnvHeight()`

**Parameters**

* none

**Returns**

|return | type | example |destination|
| :---- | :---- | :---- | :--- |
| height | Number | 1234 | success |
| nil | | | failed |

**Example**
```lua
Function mylib_GetCurRunEnvHeight()
    Local result = mylib.GetCurRunEnvHeight()
    Assert(result > 0,"GetCurRunEnvHeight err");
    Print("RunEnvHeight",result)
End
```


---

## DeleteData
Delete script database

**Prototype of function**

`mylib.DeleteData(key)`

**Parameters**

| parameter | type |
|:---------:|:------:|
| key | String |


**Returns**

Success: true; failure: false

**Example**
```lua
Function mylib_DeleteData()
    Local writeDbTbl = {
        Key = "config", --keyword
        Length = 0, -- value The total length of the data stream
        Value = {} -- value
    }
    Assert(mylib.DeleteData(writeDbTbl.key),"DeleteData err")
    Print("DeleteData return ok")
    Local readResult = {}
    readResult = {mylib.ReadData(writeDbTbl.key)}
    If(TableIsEmpty(readResult)) then
        Print("DeleteData ok")
    End
End
```

---
## GetScriptData
Get smart contract script key-value data

**Prototype of function**

`mylib.GetScriptData(paraTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| paraTbl | table |

**example**
```lua
Local paraTbl = {
    Id = {0x00, 0x01, 0x00, 0x00, 0xb7, 0xc4}, --6 bytes of smart contract script id
    Key = "config", --keyword
}
```

**Returns**


|return | type | example |destination|
| :---- | :---- | :---- | :--- |
| result | table | {0x31,0x32} | success |
| nil | | | failed |


**Example**
```lua
Function mylib_GetScriptData()

    mylib_WriteData()
    Local paraTbl = {
        --gid = {0x00,0x01,0x00,0x00,0xb7,0xc4}, --6 bytes of script id
        Id = {mylib.GetScriptID()},
        Key = "config", --keyword
    }

    Local result = {mylib.GetScriptData(paraTbl)}
    Assert(#result > 0,"GetScriptData err")
    For i = 1,#result do
        Print("GetScriptData",i,(result[i]))
    End
End
```
---
## GetUserAppAccFoudWithTag
Get the balance of the specified account

**Prototype of function**

`mylib.GetUserAppAccFoudWithTag(app_operateTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| User ID | table |

**example**
```lua
-- user id
Local idTbl = {
    idLen = 0, --id length
    idValueTbl = {} --id value
}
```

**Returns**

Success: return the transaction amount of Int64; failure: nil

**Example**
```lua
Function mylib_GetUserAppAccValue()
    Local idTbl = {
        idLen = 6, length of --id
        idValueTbl = {0x01,0x02,0x03,0x04,0x05,0x06} --id value
    }
    Local i
    Local money = { mylib.GetUserAppAccValue(idTbl) }
    Assert(#money == 8," GetUserAppAccValue err");
    For i = 1,# money do
        Print("money ",i,( money [i]))
    End
End
```

---

## IntegerToByte4
Convert Number type values ​​to 4-byte types

**Prototype of function**

`mylib.IntegerToByte4(height)`

**Parameters**

| parameter | type | Note|
|:---------:|:-----:| : :----|
| Interger | Number | 0 ~ 2^32-1|

**Returns**

Success: return a 4-byte byte stream
Failure: nil

**Example**
```lua
Function mylib_IntegerToByte()
    Local height = 1440
    Local result = {mylib.IntegerToByte4(height)}
    Assert(#result == 4,"IntegerToByte4 error0")
    Local i
    Print("height byte")
    For i = 1,#result do
        Print("",i,(result[i]))
    End
End
```

---

## Sha256
Sha256 hash encryption algorithm

**Prototype of function**

`mylib.Sha256(string))`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| Content to be encrypted | all |


**Returns**

Encrypted output, successfully returns a table, otherwise nil

**Example**
```lua
Function mylib_Sha256()
    Local orgContent = "123"
    Local content = {mylib.Sha256(orgContent)}
    LogMsg(LOG_TYPE.ENUM_NUMBER,#content,content)
End
```

---

## Des
Des encryption algorithm

**Prototype of function**

`mylib.Des(desTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| Content to be encrypted | table |

**example**
```lua
Local desTbl =
{
    dataLen = 0, --encrypted data length
    Data = {}, -- encrypt data
    keyLen = 0, -- private key length
    Key = {}, -- private key data
    Flag = 1 --1 encryption 0 decryption
}
```

**Returns**

Encrypted output, successfully returns a table, otherwise nil

**Example**
```lua
Function mylib_Des()
    Local desTbl =
    {
        dataLen = 8,
        Data = {},
        keyLen = 8,
        Key = {},
        Flag = 1
    }

    desTbl.data = {0xad, 0xdd, 0x1e, 0x1b, 0xeb, 0x8c, 0x10, 0x8d}
    For i = 1,8 do
        desTbl.key[i] = 0x33 + i
    End
    desTbl.flag = 0
    Local content = {mylib.Des(desTbl)}
    LogMsg(LOG_TYPE.ENUM_NUMBER,#content,content)
End
```

---

## VerifySignature
Verification signature

**Prototype of function**

`mylib.VerifySignature(sigTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| Content to be verified | table |

**example**
```lua
Local sigTbl =
{
    dataLen =0, -- raw data length
    Data = {}, -- raw data
    pubKeyLen = 0, --signature public key length
    pubKey = {}, --Signature public key
    signatureLen = 0, -- signature length
    Signature = {} -- signature
}
```

**Returns**

True: verification succeeded, false: failed

**Example**
```lua
Function mylib_VerifySignature()
    Local sigTbl =
    {
        dataLen = 9,
        Data = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39},
        pubKeyLen = 33,
        pubKey = {0x03, 0xee, 0xf7, 0xa3, 0x80, 0xbc, 0xf9, 0xcf, 0x97, 0x5d, 0x91, 0x6f, 0xda, 0xb1, 0x8d, 0x08, 0x1c, 0x9d, 0x55, 0xba, 0x43, 0x46, 0x54, 0x35 , 0xa4, 0xd1, 0xcc, 0x59, 0x86, 0x10, 0xa4, 0x44, 0x79},
        signatureLen = 32,
        Signature = {0x24, 0x4f, 0xa7, 0xcf, 0x97, 0xae, 0x15, 0x85, 0xd8, 0xf8, 0x02, 0x4b, 0xa1, 0x8b, 0x8a, 0xbe, 0xce, 0x8e, 0xb9, 0xcd, 0x4d, 0x01, 0x6d, 0xd0 , 0xba, 0x8c, 0xc0, 0xdc, 0x85, 0x1a, 0x9c, 0x0e}
    }

    Local ret = mylib.VerifySignature(sigTbl)
    If ret then
        LogMsg(LOG_TYPE.ENUM_STRING,string.len("ok"),"ok")
    Else
        LogMsg(LOG_TYPE.ENUM_STRING,string.len("bad"),"bad")
    End
End
```

---

## GetTxContracts
Get the contract content of the specified hash

**Prototype of function**

`mylib.GetTxContracts(hashTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| contract trading hash | table |

**Returns**

Success: return contract content; failure: nil

**Example**
```lua
Function mylib_GetTxContracts()
    Local hash = {0x24, 0x4f, 0xa7, 0xcf, 0x97, 0xae, 0x15, 0x85, 0xd8, 0xf8, 0x02, 0x4b, 0xa1, 0x8b, 0x8a, 0xbe, 0xce, 0x8e, 0xb9, 0xcd, 0x4d, 0x01, 0x6d, 0xd0, 0xba, 0x8c, 0xc0, 0xdc, 0x85, 0x1a, 0x9c, 0x0e}
    Local content = {mylib.GetTxContracts(Unpack(hash))}
    LogMsg(LOG_TYPE.ENUM_NUMBER,#content,content)
End
```

---

## TransferContactAsset
Transfer all assets

**Prototype of function**

`mylib.TransferContactAsset(Unpack(addrTbl))`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| Account Address | table |

**Returns**

Success: True; Failure: False

**Example**
```lua
Function mylib_TransferContactAsset()
    Local accountTbl = {5,157,0,0,7,34} -- 6-byte account ID
    Local addrTbl = {mylib.GetBase58Addr(Unpack(accountTbl))}
    Assert(#addrTbl > 0,"GetBase58Addr err")
    Assert(mylib.TransferContactAsset(Unpack(addrTbl)), "TransferContactAsset err")
End
```

---

## TransferSomeAsset
Transfer some assets

**Prototype of function**

`mylib.TransferSomeAsset(assetOperateTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| assetOperateTbl | table |

**example**
```lua
Local assetOperateTbl = {
    toAddrTbl = {}, --Transfer destination address
    outHeight = 0, -- height
    moneyTbl = {}, --number of assets
    fundTagLen = 0, --fund tag len
    fundTagTbl = {} --fund tag
}
```

**Returns**

Success: True; Failure: False

**Example**
```lua
Function mylib_TransferSomeAsset()
    Local accountTbl = {5,157,0,0,7,34} -- 6-byte account ID
    Local addrTbl = {mylib.GetBase58Addr(Unpack(accountTbl))}
    Assert(#addrTbl > 0,"GetBase58Addr err")
    Local assetOperateTbl = {
        toAddrTbl = {}, --Transfer address
        outHeight = 0, -- height
        moneyTbl = {}, --number of assets
        fundTagLen = 0, --fund tag len
        fundTagTbl = {} --fund tag
    }

    Local money = 1311768465173141112
    Local moneyTbl = {mylib.IntegerToByte8(money)}
    assetOperateTbl.toAddrTbl = addrTbl
    assetOperateTbl.outHeight = 1440
    assetOperateTbl.moneyTbl = moneyTbl
    Assert(mylib.TransferSomeAsset(assetOperateTbl), "TransferSomeAsset err")
End
```

---

## TransferToAddr
Transfer the WICC in the smart contract to the destination address

**Used API**

* [mylib.WriteOutput](./contract_api.md/#writeoutput)
* [mylib.GetScriptID](./contract_api.md/#getscriptid)

**Code sample**

```lua
ADDR_TYPE = {
    REGID = 1,
    BASE58 = 2
}
OP_TYPE = {
    ADD_FREE = 1,
    SUB_FREE = 2
}

WriteAccountData = function (opType, addrType, accountIdTbl, moneyTbl)
  Local writeOutputTbl = {
    addrType = addrType,
    accountIdTbl = accountIdTbl,
    operatorType = opType,
    outHeight = 0,
    moneyTbl = moneyTbl
  }
  Assert(mylib.WriteOutput(writeOutputTbl),"WriteAccountData" .. opType .. " err")
End

TransferToAddr = function (addrType, accTbl, moneyTbl)
    Assert(TableIsNotEmpty(accTbl), "WriteWithdrawal accTbl empty")
    Assert(TableIsNotEmpty(moneyTbl), "WriteWithdrawal moneyTbl empty")
    WriteAccountData(OP_TYPE.ADD_FREE, addrType, accTbl, moneyTbl)
    Local contractRegid = {mylib.GetScriptID()}
    WriteAccountData(OP_TYPE.SUB_FREE, ADDR_TYPE.REGID, contractRegid, moneyTbl)
    Return true
End
```

**Parameters**

| parameter | type | meaning | example |
|:--------- |:--------- |:---------- |:-------------- -------------------------------------------------- -------------------------------------------------- -------------------------------------------------- ---------------------------------------- |
| addrType | ADDR_TYPE | Type of address | BASE58 |
| accTbl | table | Recipient Address | {0x77, 0x4c, 0x57, 0x78, 0x43, 0x52, 0x57, 0x44, 0x54, 0x62, 0x33, 0x66, 0x55, 0x54, 0x61, 0x36, 0x7a, 0x74, 0x6f, 0x54, 0x54, 0x43, 0x7a, 0x46, 0x77, 0x44, 0x71, 0x7a, 0x62, 0x63, 0x6b, 0x53, 0x4a, 0x37} |
| moneyTbl | table | Amount | {0x00 0x50 0xd6 0xdc 0x01 0x00 0x00 0x00}|


---

## GetCurrTxPayAmount
Get the WICC amount transferred by the contract caller to the contract

**Used API**

* [mylib.GetCurTxPayAmount](./contract_api.md/#getcurtxpayamount)
* [mylib.ByteToInteger](./contract_api.md/#bytetointeger)

**Code sample**

```lua
GetCurrTxPayAmount = function ()
    Local payMoney = mylib.ByteToInteger(mylib.GetCurTxPayAmount())
    Assert(payMoney > 0,"GetCurrTxPayAmount: payMoney <= 0")
    Return payMoney
End
```
**Returns**

| type | example |
|:------ |:------- |
| Number | 10 * 10^8 (in units of `sawi`)|

---
## QueryAccountBalance
Get the WICC balance of the account

**Prototype of function**

`mylib.QueryAccountBalance(Unpack(accountTbl))`

**Parameters**

`accountTbl`: 34-byte account address

**Returns**

Success: Returns an 8-byte balance in `sawi`, which can be returned using the `mylib.ByteToInteger` method.

Failure: nil

**Example**
```lua
Function mylib_QueryAccountBalance()
    Local accountTbl = {0x77,0x4c,0x4b,0x66,0x32,0x4e,0x71,0x77,0x74,0x48,0x6b,0x33,0x42,0x66,0x7a,0x4b,0x35,0x77,
        0x4d, 0x44, 0x66, 0x62, 0x4b, 0x59, 0x4e, 0x31, 0x53, 0x43, 0x33, 0x77, 0x65, 0x79, 0x52, 0x34}

    Local result = {mylib.QueryAccountBalance(Unpack(accountTbl))}
    Assert(#result == 8,"QueryAccountBalance err");
End
```

## WriteOrModify
Write/change data

**Used API**

* [mylib.WriteData(writeTbl)](./contract_api.md/#writedata)
* [mylib.ModifyData(writeTbl)](./contract_api.md/#modifydata)

**Code sample**

```lua
WriteOrModify = function (isConfig, writeTbl)
    If not isConfig then
      If not mylib.WriteData(writeTbl) then error("WriteData error") end
    Else
      If not mylib.ModifyData(writeTbl) then error("ModifyData error") end
    End
End
```

**Parameters**

|parameter |type |meaning|
| :---- | :--- | :---- |
| isConfig | boolean | write or modify |
| writeTbl | table | data |

**Returns**

* none

---
