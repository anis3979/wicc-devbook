#Smart Contract API provides an interface between the contract and the underlying data of the node
All interface functions are encapsulated in the **mylib** library, called **Lua** language

<extoc></extoc>

---

## WriteData
Write script database

**Prototype of function**

`mylib.WriteData(writeDbTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| writeDbTbl | table |

**example**
```lua
Local writeDbTbl = {
    Key = "config", --keyword
    Length = 0, -- value The total length of the data stream
    Value = {} -- value
}

```


**Returns**

Success: successful execution; failure: nil

**Example**
```lua
Function mylib_WriteData()
    Local writeDbTbl = {
        Key = "config", --keyword
        Length = 0, -- value The total length of the data stream
        Value = {} -- value
    }

    addressTbl = {0x64, 0x63, 0x6D, 0x43, 0x62, 0x4B, 0x62, 0x41,
    0x66, 0x4B, 0x72, 0x6F, 0x66, 0x4E, 0x7A, 0x33,
    0x35, 0x4D, 0x53, 0x46, 0x75, 0x70, 0x78, 0x72,
    0x78, 0x34, 0x55, 0x77, 0x6E, 0x33, 0x76, 0x67,
    0x6A, 0x4C}

    - Each limit, daily limit, business registered account address
    writeDbTbl.value = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x64,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x2C,
    Unpack(addressTbl)}
    writeDbTbl.length = #writeDbTbl.value
    Assert(mylib.WriteData(writeDbTbl),"WriteData err")
End
```

---

## ReadData
Read the record in the script database according to the specified key

**Prototype of function**

`mylib.ReadData(key)`

**Parameters**

| parameter | type |
|:---------:|:------:|
| key | String |


**Returns**

Success: return value; failure: nil

**Example**
```lua
Function mylib_ReadData()
    Local key ="config"
    Local readResult = {mylib.ReadData(key)}
    Assert(#readResult > 0,"ReadData0 err")
    Local i
    For i = 1,#readResult do
        Print("",i,(readResult[i]))
    End
End
```

---

## ModifyData
Modify script database content

**Prototype of function**

`mylib.ModifyData(writeDbTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| writeDbTbl | table |

**example**
```lua
Local writeDbTbl = {
    Key = "config", --keyword
    Length = 0, -- value The total length of the data stream
    Value = {} -- value
}
```


**Returns**

Success: True; failure: false

**Example**
```lua
Function mylib_ModifyData()
    Local writeDbTbl = {
        Key = "config", --keyword
        Length = 0, -- value The total length of the data stream
        Value = {} -- value
    }
    addressTbl = {0x64, 0x63, 0x6D, 0x43, 0x62, 0x4B, 0x62, 0x41,
    0x66, 0x4B, 0x72, 0x6F, 0x66, 0x4E, 0x7A, 0x33,
    0x35, 0x4D, 0x53, 0x46, 0x75, 0x70, 0x78, 0x72,
    0x78, 0x34, 0x55, 0x77, 0x6E, 0x33, 0x76, 0x67,
    0x6A, 0x4C}

    - Each limit, daily limit, business registered account address
    writeDbTbl.value = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x64,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x2C,
    Unpack(addressTbl)}
    writeDbTbl.length = #writeDbTbl.value
    writeDbTbl.value[8] = 200
    Assert(mylib.ModifyData(writeDbTbl),"ModifyData err")
    Print("ModifyData ok")
    readResult = {}
    readResult = {mylib.ReadData(writeDbTbl.key)}
    Assert(#readResult > 0,"ReadData1 err")
    Print("ReadData1 ok")
    For i = 1,#readResult do
        Print("",i,(readResult[i]))
    End
End
```

---

## LogPrint
Output log information

**Prototype of function**

`mylib.LogPrint(LogTable)`

**Parameters**

| parameter | type |
|:--------- |:---- |
| What to save | table |

**example**
```lua
Local LogTable = {
    Key = 0 , --log type: LOG_TYPE.ENUM_STRING/LOG_TYPE.ENUM_NUMBER
    Length = 0, the total length of the --value data stream
    Value = nil -- string or number stream
}
```

**Returns**

* none

**Example**
```lua
Function mylib_LogPrint()

    Local LogTable = {
        Key = 0 , --log type: LOG_TYPE.ENUM_STRING/LOG_TYPE.ENUM_NUMBER
        Length = 0, the total length of the --value data stream
        Value = nil -- string or number stream
    }

    --Save the string
    LogTable.length = 9
    LogTable.value = "lua start"
    mylib.LogPrint(LogTable)

    --Save data stream
    LogTable.key = LOG_TYPE.ENUM_NUMBER
    LogTable.length = 20
    Local i
    For i = 1,20 do
        LogTable.value [i] = i
    End

    mylib.LogPrint(LogTable)
End
```



---

## WriteOutput
Operating system account for operating the WICC in the address account

**Prototype of function**

`mylib.WriteOutput(writeOutputTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| writeOutputTbl | table |


**Returns**

Success: True;

Failure: False

**Example**

Refer to [TransferToAddr](./api_debug.md/#transfertoaddr)

---
## GetScriptID
Get smart contract script ID

**Prototype of function**

`mylib.GetScriptID()`

**Parameters**

* none

**Returns**

Success: return a 6-byte script ID; failed: nil

**Example**
```lua
Function mylib_GetScriptID()
    Local result = {mylib.GetScriptID()}
    Assert(#result > 0," GetScriptID err")
    For i = 1,#result do
        Print("GetScriptID",i,(result[i]))
    End
End
```
---
## GetCurTxAccount
Get current trading account

**Prototype of function**

`mylib.GetCurTxAccount()`

**Parameters**

* none

**Returns**

Success: return account information; failure: nil

**Example**
```lua
Function mylib_GetCurTxAccount()
    Local i
    Local result = {mylib.GetCurTxAccount()}
    Assert(#result == 6," GetCurTxAccount err");
    For i = 1,#result do
        Print("Account",i,(result[i]))
    End
End
```

---
## GetCurTxInputAsset
Get the currency and amount of the current transaction transferred to the contract

**Prototype of function**

`mylib.GetCurTxInputAsset()`

**Parameters**

* none

**Returns**

Success: return a Table array
```
{
      Symbol: (string) currency symbol
      Amount: (Int) Transfer amount, unit `sawi`
}
```

Failure: nil

**Example**
```lua
Function mylib_GetCurTxInputAsset()
        
    Local CurTxInputAsset = mylib.GetCurTxInputAsset()
    Print("symbol=",CurTxInputAsset.symbol)
    Print("amount=",CurTxInputAsset.amount)
End
```

---

## GetUserAppAccValue
Get the contract token balance for the specified account

**Prototype of function**

`mylib.GetUserAppAccValue(idTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| 34 bytes base58 address | table |

**example**
```lua
-- user id
Local idTbl = {
    idLen = 0, --base58 address length
    idValueTbl = {} --base58 address
}
```

**Returns**

Success: Returns the balance value of 8 bytes in length, which can be converted to the amount of Nnmber type by the `mylib.ByteToInteger` method.
Failure: nil

**Example**
```lua
Function mylib_GetUserAppAccValue()
    Local idTbl = {
        idLen = 34, --base58 address length
        idValueTbl = {0x77,0x68,0x4d,0x31,0x64,0x4c,0x6d,0x59,0x62,0x38,0x75,0x41,0x50,0x45,0x61,0x65,0x43,0x4d,0x46,0x4b,0x72,0x74,0x37,0x46 , 0x4a, 0x57, 0x46, 0x4d, 0x64, 0x53, 0x32, 0x6a, 0x4b, 0x67} --base58 address
    }

    Local i
    Local money = { mylib.GetUserAppAccValue(idTbl) }
    Assert(#money == 8," GetUserAppAccValue err");
    For i = 1,# money do
        Print("money ",i,( money [i]))
    End
End
```

---

## WriteOutAppOperate
Operating system application account for issuing, transferring, freezing, etc. of tokens

**Prototype of function**

`mylib.WriteOutAppOperate(app_operateTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| app_operateTbl | table |

**example**
```lua
Local app_operateTbl = {
    operatorType = 0, -- operation type
    outHeight = 0, -- timeout height
    moneyTbl = {}, -- amount
    userIdLen = 0, -- account ID length
    userIdTbl = {}, -- account ID
    fundTagLen = 0, --fund tag len
    fundTagTbl = {} --fund tag
}
```

**Returns**

Success: True; failure: false

**Example**

Refer to [TransferToken](./api_debug.md/#transfertoken)


---

## GetBase58Addr
Get base58 address according to regid

**Prototype of function**

`mylib.GetBase58Addr(Unpack(accountTbl))`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| accountTbl | table |

**example**
```
--6 bytes of regid
Local accountTbl = {5,157,0,0,7,34}
```


**Returns**

Success: base58 address;

Failure: nil

**Example**
```lua
Function mylib_GetBase58Addr()
    Local accountTbl = {5,157,0,0,7,34} --6 bytes of regid
    Local result = {mylib.GetBase58Addr(Unpack(accountTbl))}
    Assert(#result == 34,"GetBase58Addr err")
End
```

---

## ByteToInteger
Convert a byte stream to a Number type

**Prototype of function**

`mylib.ByteToInteger(Unpack(byteTbl))`

**Parameters**

4-byte stream or 8-byte byte stream

**Returns**

Success: Number type
Failure: nil

**Example**
```lua
Function mylib_ByteToInteger()
    Local height = mylib.ByteToInteger(0xa0,0x05,0x00,0x00)
    Assert(height > 0, "ByteToInteger error0")
    Print("height=",height)
    Local money = mylib.ByteToInteger(0x78,0x56,0x34,0x12,0x78,0x56,0x34,0x12)
    Assert(money > 0, "ByteToInteger error1")
    Print("money=",money)
End
```

---

## IntegerToByte8
Convert Number type values ​​to 8-byte types

**Prototype of function**

`mylib.IntegerToByte8(height)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| Interger | Number |

**Returns**

Success: return a byte stream of 8 bytes
Failure: nil

**Example**
```lua
Function mylib_IntegerToByte8()
    Local money = 1311768465173141112
    Local result = {mylib.IntegerToByte8(money)}
    Assert(#result == 8,"IntegerToByte8 error0")
    For i = 1,#result do
        Print("",i,(result[i]))
    End
End
```

---

## VerifySignature
Verification signature

**Prototype of function**

`mylib.VerifySignature(sigTbl)`

**Parameters**

| parameter | type |
|:---------:|:-----:|
| Content to be verified | table |

**example**
```lua
Local sigTbl =
{
    dataLen =0, -- raw data length
    Data = {}, -- raw data
    pubKeyLen = 0, --signature public key length
    pubKey = {}, --Signature public key
    signatureLen = 0, -- signature length
    Signature = {} -- signature
}
```

**Returns**

True: verification succeeded, false: failed

**Example**
```lua
Function mylib_VerifySignature()
    Local sigTbl =
    {
        dataLen = 9,
        Data = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39},
        pubKeyLen = 33,
        pubKey = {0x03, 0xee, 0xf7, 0xa3, 0x80, 0xbc, 0xf9, 0xcf, 0x97, 0x5d, 0x91, 0x6f, 0xda, 0xb1, 0x8d, 0x08, 0x1c, 0x9d, 0x55, 0xba, 0x43, 0x46, 0x54, 0x35 , 0xa4, 0xd1, 0xcc, 0x59, 0x86, 0x10, 0xa4, 0x44, 0x79},
        signatureLen = 32,
        Signature = {0x24, 0x4f, 0xa7, 0xcf, 0x97, 0xae, 0x15, 0x85, 0xd8, 0xf8, 0x02, 0x4b, 0xa1, 0x8b, 0x8a, 0xbe, 0xce, 0x8e, 0xb9, 0xcd, 0x4d, 0x01, 0x6d, 0xd0 , 0xba, 0x8c, 0xc0, 0xdc, 0x85, 0x1a, 0x9c, 0x0e}
    }

    Local ret = mylib.VerifySignature(sigTbl)
    If ret then
        LogMsg("VerifySignature ok!")
    Else
        LogMsg("VerifySignature bad!")
    End
End
```

---
## QueryAccountBalance
Get the WICC balance of the account

**Prototype of function**

`mylib.QueryAccountBalance(Unpack(accountTbl))`

**Parameters**

`accountTbl`: 34-byte account address

**Returns**

Success: Returns an 8-byte balance in `sawi`, which can be returned using the `mylib.ByteToInteger` method.

Failure: nil

**Example**
```lua
Function mylib_QueryAccountBalance()
    Local accountTbl = {0x77,0x4c,0x4b,0x66,0x32,0x4e,0x71,0x77,0x74,0x48,0x6b,0x33,0x42,0x66,0x7a,0x4b,0x35,0x77,
        0x4d, 0x44, 0x66, 0x62, 0x4b, 0x59, 0x4e, 0x31, 0x53, 0x43, 0x33, 0x77, 0x65, 0x79, 0x52, 0x34}

    Local result = {mylib.QueryAccountBalance(Unpack(accountTbl))}
    Assert(#result == 8,"QueryAccountBalance err");
End
```

---
## GetCurTxHash
Get the current transaction hash

**Prototype of function**

`mylib.GetCurTxHash()`

**Parameters**

* none

**Returns**

|return | type | example |destination|
| :---- | :---- | :---- | :--- |
| hashTbl | table | {0x46,0x78,0xd3,0xc5,0x4c,0xaf,0x3b,0xd7,0xaf,0x59,0x1d,<br>0xb4,0x85,0x21,0xf9,0xb1,0xf6,0x98,0x12,0x90,0x9c ,0x56,0x19,<br>0xba,0x7a,0x46,0x22,0x6b,0xb7,0x2b,0x61,0xd0} | success |
| nil | | | failed |

**Example**
```lua
Function mylib_GetCurTxHash()
    Local result = {mylib.GetCurTxHash()}
    Assert(#result == 32,"GetCurTxHash err");
End
```

---
## GetTxConFirmHeight
Get the transaction confirmation height based on the transaction hash

**Prototype of function**

`mylib.GetTxConfirmHeight(Unpack(txhashTbl))`

**Parameters**

| parameter | type |
|:-------:|:-----:|
| Trading hash | table |


**Returns**

|return | type | example |destination|
| :---- | :---- | :---- | :--- |
| height | Number | 1234.0 | success |
| nil | | | failed |

**Example**
```lua
Function mylib_GetTxConfirmHeight()
    -- "hash" :"4a2af2d83683325e780f2b859e7421f4592e3105d01017aab45c15da3910be8e"
    Local txhashTbl = {0x4a,0x2a,0xf2,0xd8,0x36,0x83,0x32,0x5e,
    0x78, 0x0f, 0x2b, 0x85, 0x9e, 0x74, 0x21, 0xf4,
    0x59, 0x2e, 0x31, 0x05, 0xd0, 0x10, 0x17, 0xaa,
    0xb4, 0x5c, 0x15, 0xda, 0x39, 0x10, 0xbe, 0x8e}
    Local result = mylib.GetTxConfirmHeight(Unpack(txhashTbl))
    Assert(result > 0,"GetTxConfirmHeight err");
End
```

---

## GetBlockHash
Get the block hash based on the block height

**Prototype of function**

`mylib.GetBlockHash(height)`

**Parameters**

| parameter | type |
|:-------:|:-----:|
| Block Height | Number |


**Returns**

|return | type | example |destination|
| :---- | :---- | :---- | :--- |
| height | table | {0x46,0x78,0xd3,0xc5,0x4c,0xaf,0x3b,0xd7,0xaf,<br>0x59,0x1d,0xb4,0x85,0x21,0xf9,0xb1,0xf6,<br>0x98,0x12, 0x90,0x9c,0x56,0x19,0xba,0x7a,<br>0x46,0x22,0x6b,0xb7,0x2b,0x61,0xd0} | success |
| nil | | | failed |

**Example**
```lua
Function mylib_GetBlockHash()
    Local height = 47037
    Local result = {mylib.GetBlockHash(height)}
    Assert(#result == 32,"GetBlockHash err");
End
```
---


## GetBlockTimestamp
Get the timestamp of the block

**Prototype of function**

`mylib.GetBlockTimestamp(height)`

**Parameters**

`height` if < =0 , the actual query height is the latest block height - the absolute value of the incoming parameter;

`height` if > 0 actual query height = incoming parameters

**Returns**

Success: unix timestamp;

Failure: nil

**Example**
```lua
Function mylib_GetBlockTimestamp()
    Local ts = mylib.GetBlockTimestamp(0)
    Assert(ts ~= nil,"ts is nil")
    Str = string.format("0:%d", ts)
    LogMsg(str)
    Ts = mylib.GetBlockTimestamp(100)
    Assert(ts ~= nil,"ts is nil")
    Str = string.format("100:%d", ts)
    LogMsg(str)
    Ts = mylib.GetBlockTimestamp(-100)
    Assert(ts ~= nil,"ts is nil")
    Str = string.format("-100:%d", ts)
    LogMsg(str)
End
```

---

Public Chain 2.0 New Contract API

---
## GetCurTxInputAsset
Get the currency and incoming amount of the asset used to invoke the contract

**Prototype of function**

`mylib.GetCurTxInputAsset()`

**Parameters**

None

**Returns**

success:
```
{
    Symbol: (string), currency symbol, support but not limited to one of `WICC`, `WUSD`, `WGRT`
    Amount: (number), the currency of the incoming currency, the unit `sawi`
}
```

Failure: nil

**Example**
```lua
Function mylib_GetCurTxInputAsset()
     LogMsg("symbol:"..mylib.GetCurTxInputAsset().symbol)
     LogMsg("amount:"..mylib.GetCurTxInputAsset().amount.." sawi.")
End
```
