<extoc></extoc>
# mylib使用Instance

## myliblocal simulation library

**Description**: Since the mylib library is packaged in the chain, the **mylib local simulation library is now provided** for developers to debug locally. The debugging result of this method can't really simulate the effect of the contract on the chain.

**Usage**: The following `mylib.lua` and the development contract file must be in the same directory

`mylib.lua`:
```lua
Mylib = {}

    Mylib.constant = "test11"

    Function mylib.LogPrint(logTable)
        Print("LogPrint" , logTable.key , logTable.value)
    End

    Function mylib.GetCurTxAccount()
        Return 0x01, 0x02, 0x03, 0x04, 0x05, 0x06
    End

    Function mylib.GetBase58Addr(param)
        Return 0x77,0x54,0x77,0x72,0x57,0x73,0x65,0x72,0x37,0x38,0x6d,0x45,0x61,0x32,0x32,0x66,0x38,0x6d,0x48,0x66,0x69,0x48,0x47,0x72,0x64 , 0x4b, 0x79, 0x73, 0x54, 0x76, 0x38, 0x65, 0x42, 0x55
    End

    Function mylib.GetUserAppAccValue(param)
        Return 0x00,0x08,0xaf, 0x2f, 0x00, 0x00, 0x00, 0x00
    End

    Function mylib.ByteToInteger(param)
        Return 10000000000
    End

    Function mylib.IntegerToByte8(param)
        Return 0x0f
    End

    Function mylib.WriteOutAppOperate(param)
        Print("param.userIdLen",param.userIdLen)
        Return true
    End

    Function mylib.WriteOutput(param)
        Print("WriteOutput",param)
        Return true
    End

    Function mylib.GetScriptID()
        Return 0x97, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    End

    Function mylib.GetCurTxPayAmount()
        Return 0x97, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    End

    Function mylib.ModifyData(param)
        Print("ModifyData: param.value" , ​​param.value)
        Return true
    End

    Function mylib.WriteData(writeDbTbl)
        Print("WriteData:", writeDbTbl.value)
        Return true
    End

    Function mylib.ReadData(param)
        Print("ReadData param" , param)
        If(param=="admin") then
            Return 0x77,0x54,0x77,0x72,0x57,0x73,0x65,0x72,0x37,0x38,0x6d,0x45,0x61,0x32,0x32,0x66,0x38,0x6d,0x48,0x66,0x69,0x48,0x47,0x72,0x64 , 0x4b, 0x79, 0x73, 0x54, 0x76, 0x38, 0x65, 0x42, 0x55
        Elseif(param=="exchangeRate") then
            Return 0x00,0xb7,0x07,0x84,0x03,0x00,0x00,0x00
        End
        Return 0x97, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    End

Return mylib
```

---

## LogMsg
Output log log information

**Used API**

* [mylib.LogPrint](./contract_api.md/#logprint)

**Code sample**
```lua
LogMsg = function (msg)
    Local logTable = {
        Key = 0,
        Length = string.len(msg),
        Value = msg
    }
    mylib.LogPrint(logTable)
End
```

---

## GetCurrTxAccountAddress
Get the contract caller address

**Used API**

* [mylib.GetCurTxAccount](./contract_api.md/#getcurtxaccount)
* [mylib.GetBase58Addr](./contract_api.md/#getbase58addr)

**Code sample**
```lua
GetCurrTxAccountAddress = function ()
    Return {mylib.GetBase58Addr(mylib.GetCurTxAccount())}
End
```
**Returns**

| type | example |
|:---------- |:------------------------------------ -------------------------------------------------- -------------------------------------------------- -------------------------------------------------- ------------------ |
| table | {0x77, 0x4c, 0x57, 0x78, 0x43, 0x52, 0x57, 0x44, 0x54, 0x62, 0x33, 0x66, 0x55, 0x54, 0x61, 0x36, 0x7a, 0x74, 0x6f, 0x54, 0x54, 0x43, 0x7a, 0x46, 0x77, 0x44, 0x71, 0x7a, 0x62, 0x63, 0x6b, 0x53, 0x4a, 0x37} |

---

## GetFreeTokenCount
Get the free amount of the current account token

**Used API**

* [mylib.GetUserAppAccValue](./contract_api.md/#getuserappaccvalue)
* [mylib.ByteToInteger](./contract_api.md/#bytetointeger)

**Code sample**
```lua
GetFreeTokenCount = function (accountTbl)
    Local freeMoneyTbl = { mylib.GetUserAppAccValue( {idLen = #accountTbl, idValueTbl = accountTbl} ) }
    Assert(TableIsNotEmpty(freeMoneyTbl), "GetUserAppAccValue error")
    Return mylib.ByteToInteger(Unpack(freeMoneyTbl))
End
```

**Returns**

| type | example |
|:------ |:------- |
| Number | 100000000 |

---

## TransferToken
Transfer money to the current account token

**Used API**

* [mylib.WriteOutAppOperate](./contract_api.md/#writeoutappoperate)
* [mylib.ByteToInteger](./contract_api.md/#bytetointeger)

**Code sample**
```lua
OP_TYPE = {
    ADD_FREE = 1,
    SUB_FREE = 2
}

WriteAppData = function (opType, moneyTbl, userIdTbl)
    Local appOperateTbl = {
      operatorType = opType,
      outHeight = 0,
      moneyTbl = moneyTbl,
      userIdLen = #userIdTbl,
      userIdTbl = userIdTbl,
      fundTagLen = 0,
      fundTagTbl = {}
    }
    Assert(mylib.WriteOutAppOperate(appOperateTbl), "WriteAppData: ".. opType .." op err")
End

TransferToken = function (fromAddrTbl, toAddrTbl, moneyTbl)
    Local money = mylib.ByteToInteger(Unpack(moneyTbl))
    Assert(money > 0, money .. " <=0 error")
    Local freeMoney = GetFreeTokenCount(fromAddrTbl)
    Assert(freeMoney >= money, "Insufficient money to transfer in the account.")
    WriteAppData(OP_TYPE.SUB_FREE, moneyTbl, fromAddrTbl )
    WriteAppData(OP_TYPE.ADD_FREE, moneyTbl, toAddrTbl )
End
```

**Parameters**

| parameter | type | example |
|:-----------:|:-----:| ------- |
| fromAddrTbl | table | {0x77, 0x4c, 0x57, 0x78, 0x43, 0x52, 0x57, 0x44, 0x54, 0x62, 0x33, 0x66, 0x55, 0x54, 0x61, 0x36, 0x7a, 0x74, 0x6f, 0x54, 0x54, 0x43, 0x7a, 0x46, 0x77, 0x44, 0x71, 0x7a, 0x62, 0x63, 0x6b, 0x53, 0x4a, 0x37} |
| toAddrTbl | table | {0x77, 0x4c, 0x44, 0x6e, 0x42, 0x5a, 0x64, 0x36, 0x44, 0x6f, 0x4a, 0x56, 0x54, 0x58, 0x42, 0x4c, 0x44, 0x32, 0x4e, 0x57, 0x51, 0x34, 0x53, 0x45, 0x42, 0x52, 0x68, 0x39, 0x4e, 0x42, 0x76, 0x48, 0x50, 0x71} |
| moneyTbl | table | {0x00 0x50 0xd6 0xdc 0x01 0x00 0x00 0x00} |

**Returns**

* none


---

## UTransferToAddr
Transfer out assets within smart contracts, but not limited to `WICC` `WUSD` `WGRT`

**Code sample**

```lua
UTransferToAddr = function (tokenType, toTbl, AmountTbl)
    Assert(mylib.ByteToInteger(Unpack(AmountTbl)) ~= 0 ,"Error, contract universal transfer "..tokenType.." amount is 0!")
    Local transferTable = {
        isContractAccount = true, --(boolean, required) Is contract account or tx sender' account
        toAddressType = ADDR_TYPE.BASE58, --(number, required) The to address type of the transfer, REGID = 1, BASE58 = 2
        toAddress = toTbl, --(array, required) The to address of the transfer, array format
        tokenType = tokenType, --(string, required) Token type of the transfer, such as WICC | WUSD
        tokenAmount = AmountTbl --(array, required) Token amount of the transfer
    }
    Assert(mylib.TransferAccountAsset(transferTable) == true ,"Contract universal transfer "..tokenType.." failed!")
End
```

**Parameters**

| parameter | type | meaning | example |
|:--------- |:--------- |:---------- |:-------------- -------------------------------------------------- -------------------------------------------------- -------------------------------------------------- ---------------------------------------- |
| tokenType | String | Currency Asset Symbol | WICC WGRT WUSD |
| toTbl | table | Receiver Address | {0x77, 0x4c, 0x57, 0x78, 0x43, 0x52, 0x57, 0x44, 0x54, 0x62, 0x33, 0x66, 0x55, 0x54, 0x61, 0x36, 0x7a, 0x74, 0x6f, 0x54, 0x54, 0x43, 0x7a, 0x46, 0x77, 0x44, 0x71, 0x7a, 0x62, 0x63, 0x6b, 0x53, 0x4a, 0x37} |
| AmountTbl | table | Transfer Amount | {0x00 0x50 0xd6 0xdc 0x01 0x00 0x00 0x00}|


---


## GetContractValue
Get the value of the key corresponding to the key in the blockchain contract

**Used API**

* [mylib.ReadData](./contract_api.md/#readdata)

**Code sample**

```lua
GetContractValue = function (key)
    Assert(#key > 0, "Key is empty")

    Local tValue = { mylib.ReadData(key) }
    If TableIsNotEmpty(tValue) then
      Return true, tValue
    Else
      LogMsg("Key not exist")
      Return false,nil
    End
End
```
**Parameters**

|parameter |type |meaning|
| :---- | :--- | :---- |
| key | String | key |

**Returns**

| parameter | type | meaning | example |
|:---------- |:------- |:------------ |:------- |
| parameter1 | boolean | exist or not | true |
| parameter2 | table | value | {} |

---

## WriteStrkeyValueToDb
Write key-value key-value pairs to the blockchain

**Code sample**

```lua
WriteStrkeyValueToDb = function (Strkey, ValueTbl)
    Local t = type(ValueTbl)
    Assert(t == "table","the type of Value isn't table.")

    Local writeTbl = {
        Key = Strkey,
        Length = #ValueTbl,
        Value = {}
    }
    writeTbl.value = ValueTbl

    If not mylib.WriteData(writeTbl) then error("WriteData error") end
End
```
**Parameters**

|parameter |type |meaning|
| :---- | :--- | :---- |
| Strkey | string | key |
| ValueTbl | table | data |

**Returns**

* none

---

## GetContractAsset
Get the multi-currency balance of the contract

**Code sample**
```lua
GetContractAsset = function (tokenType)

    Local AssetTable = {
        addressType = ADDR_TYPE.REGID, -- (number, required) address type, REGID = 1, BASE58 = 2
        Address = {mylib.GetScriptID() }, -- (array, required) address, array format
        tokenType= tokenType -- (string, required) Token type of the transfer, such as WICC | WUSD
    }
    Local AssetTbl = mylib.GetAccountAsset(AssetTable)
    Assert(AssetTbl ~= nil , "GetContractAsset error!")

    Return AssetTbl
End
```

**Parameters**

| parameter | type | example |
|:-----------:|:-----:| ------- |
| tokenType | String | WICC WUSD WGRT |

**Returns**

success:
```
{
    Symbol: (string), currency symbol, support but not limited to one of `WICC`, `WUSD`, `WGRT`
    Amount: (number), the currency's corresponding balance, unit `sawi`
}
```
Failure: nil

---
## GetAddrAsset
Get the multi-currency balance of the address

**Code sample**
```lua
GetAddrAsset = function (tokenType, addrTbl)

     Local AssetTable = {
         addressType = ADDR_TYPE.BASE58, -- (number, required) address type, REGID = 1, BASE58 = 2
         Address = addrTbl, -- (array, required) address, array format
         tokenType= tokenType --(string, required) Token type of the transfer, such as WICC | WUSD
     }
     Local AssetTbl = mylib.GetAccountAsset(AssetTable)
     Assert(AssetTbl ~= nil , "GetAddrAsset error!")

     Return AssetTbl
End
```

**Parameters**

| parameter | type | example |
|:-----------:|:-----:| ------- |
| tokenType | String | WICC WUSD WGRT |

**Returns**

success:
```
{
     Symbol: (string), currency symbol, support but not limited to one of `WICC`, `WUSD`, `WGRT`
     Amount: (number), the currency's corresponding balance, unit `sawi`
}
```
Failure: nil
