# Account Initiation Guide

In order to utilize a newly created account, following steps must be taken in sequence:

## 1. create a new account (thru getnewaddr RPC or wallet client SDK API)
Sample Code (JavaScript)
```
const privateKey = wiccApi.bitcore.PrivateKey('942c9438d068b60873005147e54524b89753a5d236fed1e505d74a78ccb52ed3');
var wallet = new wiccApi.Wallet(privateKey.toWIF())
```

## 2. send some amount of coins to the new account — a must-have step!

## 3. use the account to send coins to another account for the first time
Sample Code (JavaScript)
```
var destAddr = 'wh82HNEDZkZP2eVAS5t7dDxmJWqyx9gr65' // This address helps to tell whether its testnet or mainnet
var transferAmount = 1000000 //in sawi unit
var destArr = [{
  "coinSymbol": "WICC",
  "destAddress": destAddr,
  "transferAmount": transferAmount
}
]
var cointransferTxinfo = {
  nTxType: 11,          //DO NOT MODIFY
  nValidHeight: 602371, // replace it with the latest block height which can be retrieved from Node RPC: getblockcount
  fees: 200000,         // i.e. 0.002 WICC
  srcRegId: '',         // Use RegID as a priority if available or spend 0.002 instead of 0.001 WICC for the trx fees
  dests: destArr,
  memo: "test transfer",// remark, can be omitted at all
  feeSymbol: "WICC"
};

var transaction = new WaykiTransaction(cointransferTxinfo, wallet)
var cointransferTx = transaction.genRawTx()
```

## 4. use the account to send coins to other accounts using RegID as the source address
Same as #3 but use RegID of the source account in srcRegId field.
