

When you need to pick up the receipt for the transaction details, please refer to the configuration file description to `genreceipt=1`

| Transaction type  (API `gettxdetail` can be checked)| Description | fee(`wi`) | Remarks |
|:---------------------------------|:------------- -------------|:----------------------------------- ---------------------| :---- |
"ACCOUNT_REGISTER_TX" | Account Activation | 0.1|The use of the public chain v1.0, the public chain v2.0 is still reserved, but it is not recommended to use the initiative to initiate a transaction to activate the account |
| "BCOIN_TRANSFER_TX" | WICC Transfer | 0.1| Usage of public chain v1.0, public chain v2.0 still supported (not recommended) |
| "UCOIN_TRANSFER_TX" | Multi-currency Transfer |0.001| New use of public chain v2.0, support but not limited to WICC, WUSD, WGRT |
| "UCOIN_TRANSFER_MTX" | Multi-Signature Transfer | 0.1| New use of public chain v2.0|
| "LCONTRACT_DEPLOY_TX" | Lua Contract Deployment | 1| The use of the public chain v1.0, the public chain v2.0 still supports (not recommended) |
| "UCONTRACT_DEPLOY_TX" | Lua, WASM Contract Deployment | 1| Public Chain v2.0 New Usage (Recommended) |
| "LCONTRACT_INVOKE_TX" | Lua Contract Call | 0.01| The use of the public chain v1.0, the public chain v2.0 still supports (not recommended) |
| "UCONTRACT_INVOKE_TX" | Lua, WASM Contract Call | 0.01| Public Chain v2.0 New Usage (Recommended) |
| "UCOIN_STAKE_TX" | Multi-currency Mortgage | 0.01| |
| "DELEGATE_VOTE_TX" | DPOS Voting / Withdrawal | 0.01| |
| "ASSET_ISSUE_TX" | Digital Assets Release | 0.01| New features in the public chain v2.0, support for publishing underlying digital assets under the public chain, need to pay 550WICC as a currency fee |
| "ASSET_UPDATE_TX" | Digital Asset Update | 0.01|New features of the public chain v2.0, support for updating the underlying digital assets based on the public chain, need to pay 110WICC as the update fee |
| "PRICE_FEED_TX" | Feed Price | 0.001| |
| "CDP_STAKE_TX" | User CDP Mortgage | 0.01| |
| "CDP_REDEEM_TX" | User CDP Redemption | 0.01| |
| "CDP_LIQUIDATE_TX" | User CDP Clearing | 0.01| |
| "DEX_LIMIT_BUY_ORDER_TX" | DEX Limit Buy Order | 0.001| |
| "DEX_LIMIT_SELL_ORDER_TX" | DEX Limit Selling List | 0.001| |
| "DEX_MARKET_BUY_ORDER_TX" | DEX Market Pending Order | 0.001| |
| "DEX_MARKET_SELL_ORDER_TX" | DEX Marketable List | 0.001| |
| "DEX_CANCEL_ORDER_TX" | DEX Cancel Pending Order | 0.001| |
| "DEX_TRADE_SETTLE_TX" | DEX Joint Purchase Order | 0.0001| |

## Multi-currency transaction unit conversion, the minimum unit is `sawi`
**The accuracy of all digital asset units on the WaykiChain chain is unified to `8`, including not limited to `WUSD`, `WGRT`, `WICC`, `WRC20 tokens, etc.**
![](../images/decimal.png)
