#### Smart Contract API 

<br>

---

## `submitcontractdeploytx`

Contract for the deployment of Intelligent any contract addresses can publish intelligent, smart contract publisher is not necessarily the owner of the contract.

The deployed smart contract file must be in the /tmp/lua/path of the node running environment .

**Parameters**

`address` The address of the person who issued the smart contract

`filepath` Published smart contract file path + file name

`fee` Intelligent release required fee contract minimum 110000000 sawi

**Returns**

hash Publish a hash of a contract transaction

**Example**

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"submitcontractdeploytx","params":["wd3hLkmd5Jrmck4Rsg8WCcJ3PfFrKFDbbA","/tmp/lua/lotteryV3.lua",110000000]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result":
    {
        "hash":"641794c5db4af756660197878dce462b887224a2efad8387d915bbc2acb5aa9d"
    },
    "error":null,
    "id":"curltext"
}
```

<br>

---

## `getcontractregid`

Get an object containing regid

**Parameters**

`txhash` the transaction hash

**Returns**

`regid`    smart contract regid
`regid_hex` Smart contract regid into hex

**Example**

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"getcontractregid","params":["641794c5db4af756660197878dce462b887224a2efad8387d915bbc2acb5aa9d"]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "regid": "48493-2",
        "regid_hex": "6dbd00000200"
    },
    "error": null,
    "id": "curltext"
}
```

<br>

---


## `getcontractaccountinfo`

Get information about users in smart contracts

**Parameters**

`regid` regid of smart contract

`address` Queryed user address /regid

**Returns**

`account_uid` Query the user address into a hex string

`free_value` The number of free and operable tokens for smart contracts

`frozen_funds` Smart contract's number of frozen coins and data list for thawing time

**Example**

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"getcontractaccountinfo","params":["48493-2","wMHkGQKHf4CXdZRRoez8b9YgYnPzGxbs3j"]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "account_uid": "774d486b47514b4866344358645a52526f657a3862395967596e507a47786273336a",
        "free_value": 0,
        "frozen_funds": []
    },
    "error": null,
    "id": "curltext"
}
```

<br>

---

## `getcontractdata`

get the app data by given regid.

**Parameters**

`regid` smart contract regid

`key`   the key of data in smart contract

`hexadecimal_format` Whether key and value are in hex format

```
`true`: hex format 
`false` not hex format
```

**Returns**

`regid`  smart contract regid

`key`  the key of data in smart contract

`value` the value of data in smart contract

**Example**

```json
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"getcontractdata","params":["50552-2","owner",false]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "contract_regid": "50552-2",
        "key": "owner",
        "value": "wMHkGQKHf4CXdZRRoez8b9YgYnPzGxbs3j"
    },
    "error": null,
    "id": "curltext"
}

```

<br>

---


## `getcontractinfo`

get smart contract information

**Parameters**

`regid` smart contract regid

**Returns**

`contract_regid` Smart contract regid

`vm_type` Virtual machine type for contract operation

`upgradable`

`code` Smart contract code (hex)

`memo` Remarks when publishing a contract

`abi` Abi information

**Example**

```json
// Request
curl -u Waykichain:admin -d '{"jsonrpc":"2.0","id":"curltext","method":"getcontractinfo","params":["49100-2"]}' -H 'content-type:application/json;' http://127.0.0.1:6967

// Response
{
    "result": {
        "contract_regid": "49100-2",
        "vm_type": 1,
        "upgradable": true,
        "code": "6d796c6962203d207265717569726520226d796c6962220a0a4d61696e203d2066756e6374696f6e28290a0a656e640a0a4d61696e28290a0a",
        "memo": "",
        "abi": ""
    },
    "error": null,
    "id": "curltext"
}
```

<br>

---
