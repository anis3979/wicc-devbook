# Smart Contract API -- WASM    


## submitsetcodetx  

Contract for the deployment of Intelligent any contract addresses can publish intelligent, smart contract publisher is not necessarily the owner of the contract.  

**Parameters**  

submitsetcodetx "sender" "wasm_file" "abi_file" ["contract"] [symbol:fee:unit]    
deploy code and abi to an account as contract  

Arguments:  

`sender`             (string, required), contract regid address from this wallet

`code_file`       (string, required), the file path of the contract code  

`abi_file`         (string, required), the file path of the contract abi  

`contract`         (string, optional), existing contract regid, omitted for new deployment.  

`symbol:fee:unit` (string:numeric:string, optional) fee paid to miner, default is WICC:100000:sawi  

**Result**  
`txhash`           (string) Publish a hash of a contract transaction  

**Examples**  
```
// Request  
root@0e7612ac5bc5:/tmp/wasm# coind submitsetcodetx 0-2 /tmp/wasm/hello/hello.wasm /tmp/wasm/hello/hello.abi  

// Response  
{
    "txid" : "3e5f9f31cdcf32ff93f4f8741b1fc467721b93a0282e7f5bea758096ffb49068"  
}  
```

```
// json  
 curl --user myusername -d '{"jsonrpc": "1.0", "id":"curltest", "method":"submitsetcodetx", "params":["0-2", "/tmp/wasm/hello/hello.wasm", "/tmp/wasm/hello/hello.abi"]}' -H 'Content-Type: application/json;' http://127.0.0.1:8332
```

---
## getcontractregid  

get the regid of smart contract  

**Parameters**  

`txhash`       The transaction hash  

**Returns**  

`regid`         Smart contract regid  

`regid_hex` Smart contract regid into hex   

**Example**

```
//Request
root@0e7612ac5bc5:/tmp/wasm# coind getcontractregid "3e5f9f31cdcf32ff93f4f8741b1fc467721b93a0282e7f5bea758096ffb49068"  

// Response  
{
    "regid" : "9925-2",  
    "regid_hex" : "c52600000200"  
}

```

---

## submittx  
call by smart contract   

**Parameters**

submittx "sender" "contract" "action" "data" "fee"  

`sender`           (string, required) sender regid  

`contract`       (string, required) contract regid   

`action`           (string, required) action name  

`data`               (json string, required) action data  

`symbol:fee:unit` (numeric, optional) pay to miner  

**Result**  

`txid`:       (string)Call a hash of a contract transaction  

**Examples**  

```
// Request  
root@0e7612ac5bc5:/tmp/wasm# coind submittx 0-2 9925-2 hi '["kj"]'

// Response
{
    "txid" : "29a779176018e4f1bb8aaf58d89aa92d73bc0e73e62c88470c6c7db6664cc8e4",
    "tx_trace" : {
        "trx_id" : "29a779176018e4f1bb8aaf58d89aa92d73bc0e73e62c88470c6c7db6664cc8e4",
        "elapsed" : 262,
        "minimum_fee" : 100000,
        "traces" : [
            {
                "trx_id" : "29a779176018e4f1bb8aaf58d89aa92d73bc0e73e62c88470c6c7db6664cc8e4",
                "receiver" : "9925-2",
                "trx" : {
                    "contract" : "9925-2",
                    "action" : "hi",
                    "authorization" : [
                        {
                            "account" : "0-2",
                            "permission" : "wasmio_owner"
                        }
                    ],
                    "data" : {
                        "nm" : "kj"
                    }
                }
            }
        ]
    }
}
```
